//
//  main.m
//  TestApp
//
//  Created by Pravin Tawade on 11/07/14.
//  Copyright (c) 2014 tieto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
