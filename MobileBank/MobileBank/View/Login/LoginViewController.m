/*************************************************************************
 *
 *  Tieto Confidential
 * ______________________________
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of Tieto.
 *
 *  LoginViewController.m
 *  Used to display login view/page
 *
 *  Created by Pravin Tawade on 04/11/14..
 *  Copyright 2014 Tieto. All rights reserved.
 **************************************************************************/

#import "LoginViewController.h"
#import "ConnectorService.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

#pragma mark - View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    //TODO: To test request using lib connector uncomment this line
//    ConnectorService *service = [[ConnectorService alloc] init];
//    [service sendTestRequest];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
