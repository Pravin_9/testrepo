/*************************************************************************
 *
 *  Tieto Confidential
 * ______________________________
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of Tieto.
 *
 *  LoginViewController.h
 *  Used to display login view/page
 *
 *  Created by Pravin Tawade on 04/11/14..
 *  Copyright 2014 Tieto. All rights reserved.
 **************************************************************************/

#import <UIKit/UIKit.h>

/*!
 * @class       LoginViewController
 * @brief       This is the first page on launching the app
 * @discussion  First page on launching the app. Users can login with different type 
 */
@interface LoginViewController : UIViewController

@end
