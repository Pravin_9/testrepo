/*************************************************************************
 *
 *  Tieto Confidential
 * ______________________________
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of Tieto.
 *
 *  AppDelegate.h
 *  Application delegate created by framework
 *
 *  Created by Pravin Tawade on 04/11/14..
 *  Copyright 2014 Tieto. All rights reserved.
 **************************************************************************/


#import <UIKit/UIKit.h>


/*!
 * @class       AppDelegate
 * @brief       Application delegate class, starting point of the application
 * @discussion  launches the first view of application, handles switching between the views
 */
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window; /*!< property variable for application windows */


@end

