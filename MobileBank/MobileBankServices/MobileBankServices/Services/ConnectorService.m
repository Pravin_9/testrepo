/*************************************************************************
 *
 *  Tieto Confidential
 * ______________________________
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of Tieto.
 *
 *  ConnectorService.m
 *  This class will contain the implementation and webservice call using connector lib.
 *  This will contain configurations for connector lib for GET/POST calls and certificate validation if required.
 *  This will be called from the Async task by ServiceManager class.
 *  This API call will return the response in JSON format and that response will be handled by the caller of the service through ServiceManager class.
 *
 *  Created by Pravin Tawade on 04/11/14.
 *  Copyright 2014 Tieto. All rights reserved.
 **************************************************************************/

#import "ConnectorService.h"

@implementation ConnectorService


#pragma mark - Private Methods.
- (LibConnector *) getLibConnectorInstance {
    LibConnector *connector = [[LibConnector alloc] init];
    [connector setUsername:@"mobileuser@tieto.com"];
    [connector setPassword:@"mobileuser"];
    [connector setServerAddress:@"https://fdc.tmg.portal.tieto.com/mobile/services/connector"];
    [connector setDelegate:self];
    return connector;
}


#pragma mark - Public method
- (void)sendTestRequest {
    
    //Instantiate the connection object
    serviceLibConnector = [self getLibConnectorInstance];
    
    //create service request.
    ServiceRequest *request = [[ServiceRequest alloc] init];
    //set test service name.
    [request setServiceName:@"tomp.authenticator/authenticate"];
    
    //sent get request.
    [serviceLibConnector GET:request];
}


#pragma mark - Lib Connector delegate methods

- (void)request:(LibConnector *)request onStart:(int)requestId
{
    NSLog(@"LibConnectorTestApp - onStart() -> %d", requestId);
}


- (void) request:(LibConnector *)request onFinnish:(int)requestId
{
    NSLog(@"LibConnectorTestApp - onFinnish() -> %d", requestId);
}

- (void)request:(LibConnector *)request onSuccess:(NSObject *)response requestId:(int)requestId
{
    NSLog(@"LibConnectorTestApp - onSuccess() -> %d             response:%@", requestId, response);
}


- (void) request:(LibConnector *)request onFailure:(int)errorCode response:(NSObject *)response requestId:(int)requestId
{
    NSLog(@"LibConnectorTestApp - onFailure() errorCode = %d -> %d      response:%@", errorCode, requestId,response);
    
}


@end
