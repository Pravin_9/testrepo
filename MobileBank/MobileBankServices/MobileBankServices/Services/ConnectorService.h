/*************************************************************************
 *
 *  Tieto Confidential
 * ______________________________
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of Tieto.
 *
 *  ConnectorService.h
 *  This class will contain the implementation and webservice call using connector lib.
 *  This will contain configurations for connector lib for GET/POST calls and certificate validation if required.
 *  This will be called from the Async task by ServiceManager class.
 *  This API call will return the response in JSON format and that response will be handled by the caller of the service through ServiceManager class.
 *
 *  Created by Pravin Tawade on 04/11/14.
 *  Copyright 2014 Tieto. All rights reserved.
 **************************************************************************/

#import <Foundation/Foundation.h>
#import "LibConnector.h"

/*!
 * @class       ConnectorService
 * @brief       This class will contain the implementation and webservice call using connector lib.
 * @discussion  This will contain configurations for connector lib for GET/POST calls and certificate validation if required.
 *              This API call will return the response in JSON format and that response will be handled by the caller of the service through ServiceManager class.
 */
@interface ConnectorService : NSObject<LibConnectorDelegate> {
    
    LibConnector *serviceLibConnector;
}

#pragma mark - Public method
/*!
 * @class       sendTestRequest
 * @brief       This method will create instance of lib connect and service request to Test authentication reuqest.
 * @discussion  None
 */
- (void)sendTestRequest;

@end
