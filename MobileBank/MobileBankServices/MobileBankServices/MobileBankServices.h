/*************************************************************************
 *
 *  Tieto Confidential
 * ______________________________
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of Tieto.
 *
 *  MobileBankServices.h
 *
 *
 *  Created by Pravin Tawade on 04/11/14.
 *  Copyright 2014 Tieto. All rights reserved.
 **************************************************************************/

#import <UIKit/UIKit.h>

//! Project version number for MobileBankServices.
FOUNDATION_EXPORT double MobileBankServicesVersionNumber;

//! Project version string for MobileBankServices.
FOUNDATION_EXPORT const unsigned char MobileBankServicesVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MobileBankServices/PublicHeader.h>


