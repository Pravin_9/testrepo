/*************************************************************************
 *
 *  Tieto Confidential
 * ______________________________
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of Tieto.
 *
 *  BaseServiceManager.h
 *  This will be used to do basic setup and configuration for API calls using Connector lib.
 *
 *  Created by Pravin Tawade on 04/11/14.
 *  Copyright 2014 Tieto. All rights reserved.
 **************************************************************************/

#import <Foundation/Foundation.h>

/*!
 * @class       BaseServiceManager
 * @brief       This will be used to do basic setup and configuration for API calls using Connector lib.
 * @discussion  This class will be responsible for calling the APIs in Async task and give callback for the request status
 */
@interface BaseServiceManager : NSObject

@end
