//
//  LibConnector.h
//  LibConnector
//
//
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


/**
 * The library encountered an unexpected condition that can not be handled
 */
#define LIBCONNECTOR_INTERNAL_ERROR 9999

/**
 * Authentication error: Invalid or missing user, password, company domain or unclassified authorization error
 */
#define TOMP_ERROR_AUTHENTICATION_FAILED 10001

/**
 * Authentication error: Missing UDID
 */
#define TOMP_ERROR_UDID_MISSING 10002

/**
 * Authentication error: User device is disabled or maximum number of devices for user has reached
 */
#define TOMP_ERROR_UDID_DISABLED 10003

/**
 * Authentication error: User device is not bound to user
 */
#define TOMP_ERROR_UDID_NOT_BOUND 10004

/**
 * Unclassified security error
 */
#define TOMP_ERROR_UNCLASSIFIED_SECURITY_ERROR 10005

/**
 * Unclassified ReceiverForEndpointException or ResourceNotFoundException type error
 */
#define TOMP_ERROR_UNCLASSIFIED_NOT_FOUND 10006

/**
 * Service is not found
 */
#define TOMP_ERROR_SERVICE_NOT_FOUND 10007

/**
 * Unable to find Processor configured for TOMP connector
 */
#define TOMP_ERROR_MISSING_PROCESSOR 10008

/**
 * Post processing failed
 */
#define TOMP_ERROR_POST_PROCESSING_FAILED 10009

/**
 * Connector was not found by service name
 */
#define TOMP_ERROR_CONNECTOR_NOT_FOUND 10010

/**
 * Required URL parameter defined in TOMP connector configuration is not mapped with any value
 */
#define TOMP_ERROR_UNMAPPED_URL_PARAM 10011

/**
 * Unclassified NotPermittedException type error
 */
#define TOMP_ERROR_UNCLASSIFIED_NOT_PERMITTED 10012

/**
 * Unclassified RoutePathNotFoundException or FilterUnacceptedException type error
 */
#define TOMP_ERROR_UNCLASSIFIED_ROUTE_NOT_FOUND 10013

/**
 * Unclassified Exception, RuntimeException on any internal server error
 */
#define TOMP_ERROR_UNCLASSIFIED_INTERNAL_ERROR 10014

/**
 * Transformation error
 */
#define TOMP_ERROR_TRANSFORMATION_ERROR 10015

/**
 * One time password needed for authentication
 */
#define TOMP_ERROR_OTP_NEEDED 10016

/**
 * Authentication error: Invalid OTP token
 */
#define TOMP_ERROR_OTP_AUTH_FAILED 10017

/**
 * Authentication error: OTP Chanllenge (e.g. next token needed)
 */
#define TOMP_ERROR_OTP_AUTH_CHALLENGE 10018

/**
 * Authentication error: Error in OTP authentication
 */
#define TOMP_ERROR_OTP_AUTH_ERROR 10019

/**
 * Unknown error
 */
#define TOMP_ERROR_UNKNOWN 10901

/**
 * Unknown 401 error
 */
#define TOMP_ERROR_UNKNOWN_401 10902

/**
 * Unknown 403 error
 */
#define TOMP_ERROR_UNKNOWN_403 10903

/**
 * Unknown 404 error
 */
#define TOMP_ERROR_UNKNOWN_404 10904

/**
 * Unknown 405 error
 */
#define TOMP_ERROR_UNKNOWN_405 10905

/**
 * Unknown 406 error
 */
#define TOMP_ERROR_UNKNOWN_406 10906

/**
 * Unknown 500 error
 */
#define TOMP_ERROR_UNKNOWN_500 10907


#pragma mark -
#pragma mark - ServiceRequest
/**
 * ServiceRequest is the collection of the request parameters
 * and possible payload data.
 *
 */ 
@interface ServiceRequest : NSObject {
    
}
@property (nonatomic, retain) NSString *serviceName;
@property (nonatomic, retain) NSMutableDictionary *parameters;
@property (nonatomic, retain) NSObject *payload;
@property (nonatomic, retain) NSString *payloadMIMEType;

/**
 * Adds one key-value pair parameter.
 * 
 * @param key
 * @param value
 */
- (void)addParameterForKey:(NSString *)key withValue:(NSString *)value;

/**
 * Removes one parameter from parameter dictonary.
 * 
 * @param key
 */
- (void)removeParameter:(NSString *)key;

/**
 * Adds dictonary of key-value pair parameters.
 * 
 * @param parameters
 */
- (void)addParameters:(NSMutableDictionary *)parameters;

/**
 * Sets the payload data in NSString format.
 * 
 * @param payload
 */
- (void)setPayloadString:(NSString *)payload;

/**
 * Sets the payload data in NSData format.
 * 
 * @param payload
 */
- (void)setPayloadData:(NSData *)payload;

@end


#pragma mark -
#pragma mark - LibConnector
/**
 * The LibConnector provides asynchronous GET, POST, PUT and DELETE methods for
 * communication with TOMP server. It handles parameters mapping to request
 * as well as automatic user-agent generation and device UDID gathering.
 * The library supports text and binary data format in payload.
 *
 */
@interface LibConnector : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate> {

}
    /**
     The username used for authentication.
    */
    @property (nonatomic, retain) NSString *username;

    /**
     The password used for authentication.
     */
    @property (nonatomic, retain) NSString *password;

    /**
     The service server address.
    */
    @property (nonatomic, retain) NSString *serverAddress;

    /**
     The one time password user for authentication. Not used if not set.
     This will be set back to null when the value has been copied to the request.
    */
    @property (nonatomic, retain) NSString *oneTimePassword;

    /**
     The connection time out in seconds. If not set, the default value is 10 seconds.
    */
    @property int timeout;

/**
 * Performs authentication request against to server
 *
 * @return requestId
 */
- (int)doAuthentication;

/**
 * Performs a GET request to server.
 * 
 * @param serviceRequest
 * @return requestId
 */
- (int)GET:(ServiceRequest *)serviceRequest;

/**
 * Performs a POST request to server.
 * 
 * @param serviceRequest
 * @return requestId
 */
- (int)POST:(ServiceRequest *)serviceRequest;

/**
 * Performs a PUT request to server.
 * 
 * @param serviceRequest
 * @return requestId
 */
- (int)PUT:(ServiceRequest *)serviceRequest;

/**
 * Performs a DELETE request to server.
 * 
 * @param serviceRequest
 * @return requestId
 */
- (int)DELETE:(ServiceRequest *)serviceRequest;

/**
 * Stores username in secure way
 *
 * @param username
 */
- (void)storeUsername:(NSString *)username;

/**
 * Stores password in secure way
 *
 * @param password
 */
- (void)storePassword:(NSString *)password;

/**
 * Returns username from secure storage
 *
 * @return username
 */
- (NSString *)getStoredUsername;

/**
 * Returns password from secure storage
 *
 * @return password
 */
- (NSString *)getStoredPassword;

/**
 * Cleans username from secure storage
 *
 */
- (void)cleanStoredUsername;

/**
 * Cleans password from secure storage
 *
 */
- (void)cleanStoredPassword;

/**
 * Cleans all credentials from secure storage
 *
 */
- (void)cleanStoredCredentials;

@end


#pragma mark -
#pragma mark - LibConnectorDelegate
/**
 * Delegate for LibConnector.
 *
 */
@protocol LibConnectorDelegate <NSObject>

@optional

/**
 * Delegate method, executed when the request is started. 
 * This is the first step on request lifecyle.
 *
 * @param requestId
 */
- (void)request:(LibConnector *)request onStart:(int)requestId;

/**
 * Delegate method, executed when the request is finished. 
 * This is the last step on request lifecyle.
 *
 * @param requestId
 */
- (void)request:(LibConnector *)request onFinnish:(int)requestId;

/**
 * Delegate method, executed when the request responses succesfully.
 * 
 * @param response
 * @param requestId
 */
- (void)request:(LibConnector *)request onSuccess:(NSObject *)response requestId:(int)requestId;

/**
 * Delegate method, executed when the request fails,
 * 
 * @param errorCode
 * @param response 
 * @param requestId
 */
- (void)request:(LibConnector *)request onFailure:(int)errorCode response:(NSObject *)response requestId:(int)requestId;

@end


@interface LibConnector ()

    @property (retain, nonatomic) id<LibConnectorDelegate> delegate;

@end
