/*************************************************************************
 *
 *  Tieto Confidential
 * ______________________________
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of Tieto.
 *
 *  DataStorage.h
 *  This utility class will store constants and common data for the application.
 *
 *  Created by Pravin Tawade on 04/11/14.
 *  Copyright 2014 Tieto. All rights reserved.
 **************************************************************************/

#import <Foundation/Foundation.h>

/*!
 * @class       DataStorage
 * @brief       This utility class will store constants and common data for the application.
 * @discussion  For storage, default encryption methods will be used while storing in SharedPrefs
 */
@interface DataStorage : NSObject

@end
