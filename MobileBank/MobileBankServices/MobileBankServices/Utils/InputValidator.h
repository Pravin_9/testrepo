/*************************************************************************
 *
 *  Tieto Confidential
 * ______________________________
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of Tieto.
 *
 *  InputValidator.h
 *  This will contain common input validations for the app.
 *
 *  Created by Pravin Tawade on 04/11/14.
 *  Copyright 2014 Tieto. All rights reserved.
 **************************************************************************/

#import <Foundation/Foundation.h>

/*!
 * @class       InputValidator
 * @brief       This will contain common input validations for the app.
 * @discussion
 */
@interface InputValidator : NSObject

@end
