/*************************************************************************
 *
 *  Tieto Confidential
 * ______________________________
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of Tieto.
 *
 *  MobileBankUtils.m
 *  This will contain common utility functions for the app.
 *
 *  Created by Pravin Tawade on 04/11/14.
 *  Copyright 2014 Tieto. All rights reserved.
 **************************************************************************/

#import "MobileBankUtils.h"

@implementation MobileBankUtils

@end
