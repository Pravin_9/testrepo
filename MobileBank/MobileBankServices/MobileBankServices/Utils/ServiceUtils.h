/*************************************************************************
 *
 *  Tieto Confidential
 * ______________________________
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of Tieto.
 *
 *  ServiceUtils.h
 *  Used to add Service call and Connector lib access related utility functions
 *
 *  Created by Pravin Tawade on 04/11/14.
 *  Copyright 2014 Tieto. All rights reserved.
 **************************************************************************/

#import <Foundation/Foundation.h>

/*!
 * @class       ServiceUtils
 * @brief       Used to add Service call and Connector lib access related utility functions
 * @discussion
 */
@interface ServiceUtils : NSObject

@end
