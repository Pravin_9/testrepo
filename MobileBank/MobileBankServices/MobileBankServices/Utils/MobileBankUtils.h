/*************************************************************************
 *
 *  Tieto Confidential
 * ______________________________
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of Tieto.
 *
 *  MobileBankUtils.h
 *  This will contain common utility functions for the app.
 *
 *  Created by Pravin Tawade on 04/11/14.
 *  Copyright 2014 Tieto. All rights reserved.
 **************************************************************************/

#import <Foundation/Foundation.h>

/*!
 * @class       NetBankUtils
 * @brief       This will contain common utility functions for the app.
 * @discussion  
 */
@interface MobileBankUtils : NSObject

@end
