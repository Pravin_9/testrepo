/*************************************************************************
 *
 *  Tieto Confidential
 * ______________________________
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of Tieto.
 *
 *  ResponseModel.h
 *  This will contain UI and web service request specific parser classes will be created as per API doc
 *
 *  Created by Pravin Tawade on 04/11/14.
 *  Copyright 2014 Tieto. All rights reserved.
 **************************************************************************/


#import <Foundation/Foundation.h>

/*!
 * @class       ResponseModel
 * @brief       This will contain UI and web service request specific parser classes will be created as per API doc
 * @discussion
 */
@interface ResponseModel : NSObject

@end
